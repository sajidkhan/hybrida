FROM python:3.8.13-alpine3.16

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc g++ libc-dev linux-headers postgresql-dev
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /requirements.txt
RUN apk del .tmp-build-deps


RUN mkdir /app
WORKDIR /app
COPY . /app

RUN addgroup -g 1024 -S django && adduser -u 112 -S django
RUN chown -R django /app
USER django
